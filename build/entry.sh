#!/bin/sh
set -e

sed -i "s|GITLAB_HOST|$GITLAB_HOST|g" /var/www/client/web/assets/v1.2.0/js/app.min.js

exec "$@"
