exports.httpBackendMock = function (mocks) {
    angular.module('gitlabKBAppMock', [
        'gitlabKBApp',
        'ngMockE2E'
    ]).run([
        '$httpBackend',
        function ($httpBackend) {
            $httpBackend.whenGET('/api/boards').respond(mocks.boards);

            $httpBackend.whenGET('/api/board?project_id=kanban%2Fclient').respond(mocks.board);
            $httpBackend.whenGET('/api/labels?board_id=83866').respond(mocks.labels);
            $httpBackend.whenGET('/api/cards?project_id=83866').respond(mocks.cards);
            $httpBackend.whenGET('/api/milestones?project_id=83866').respond(mocks.milestones);
            $httpBackend.whenGET('/api/users?project_id=83866').respond(mocks.users);
            $httpBackend.whenGET('/api/comments?issue_id=146614&project_id=83866').respond(mocks.comments);


            $httpBackend.whenPOST('/api/login').respond(function (method, url, data) {
                var data1 = angular.fromJson(data);
                if (data1._username === data1._password) {
                    return [
                        200,
                        mocks.login
                    ];
                }

                return [
                    400,
                    {
                        'success': false,
                        'error': 'Invalid credentials'
                    }
                ];
            });

            $httpBackend.whenGET(/assets\//).passThrough();
            $httpBackend.whenGET(/template\//).passThrough();
        }
    ]);
};
