var boards = require('./boards.json');
var board = require('./board.json');
var login = require('./login.json');
var cards = require('./cards.json');
var labels = require('./labels.json');
var users = require('./users.json');
var milestones = require('./milestones.json');
var comments = require('./comments.json');

exports.mocks = {
    boards: boards,
    board: board,
    login: login,
    cards: cards,
    labels: labels,
    users: users,
    milestones: milestones,
    comments: comments
};
