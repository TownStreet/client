var SigninPage = require('./signin_page_spec.js');
var mockModule = require('./../../application.mock.js');

var mocks = require('./../../mocks/mocks.js').mocks;

describe('Kanban user module', function () {
    beforeEach(function () {
        browser.addMockModule('ngMockE2E', function () {
            var js = document.createElement('script');

            js.type = 'text/javascript';
            js.src = '/assets/js/angular-mocks.js';

            document.body.appendChild(js);
        });

        browser.addMockModule('gitlabKBAppMock', mockModule.httpBackendMock, mocks);
    });

    it('should show warning if could not authenticate user', function () {
        var signinPage = new SigninPage();

        signinPage.get();

        browser.waitForAngular();

        signinPage.setLogin('qwerty1').setPassword('qwerty');
        signinPage.button.click();

        browser.waitForAngular();

        expect(browser.getLocationAbsUrl()).toEqual('/');
        expect(element(by.css('.error')).getText()).toEqual('Invalid credentials');
    });

    it('should show boards after successful sign in', function () {
        var signinPage = new SigninPage();
        signinPage.get().setLogin('qwerty').setPassword('qwerty');
        signinPage.button.click();

        browser.waitForAngular();

        expect(browser.getLocationAbsUrl()).toEqual('/boards/');
    });

});
